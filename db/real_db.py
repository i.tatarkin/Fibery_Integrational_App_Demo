from tinydb import TinyDB
from models.cv_model import CVModel, SkillModel, LangSkillModel

cv_db = TinyDB("/home/veve/PycharmProjects/fibery_synchro_last_test/db/cv_db.json")
skill_db = TinyDB("/home/veve/PycharmProjects/fibery_synchro_last_test/db/skill_db.json")
lang_skill_db = TinyDB("/home/veve/PycharmProjects/fibery_synchro_last_test/db/lang_skill_db.json")


def all_data_from_cv_db(db: TinyDB):
    all_cvs = db.all()
    data = []
    for i in range(len(all_cvs)):
        cv = CVModel(
            name=all_cvs[i]['name'],
            id=all_cvs[i]['id'],
            description=all_cvs[i]['description'],
            CareerStart=all_cvs[i]['CareerStart'],
            PDF=all_cvs[i]['PDF']
                     )
        cv = cv.dict(by_alias=True)
        data.append(cv)
    return data


def all_data_from_skill_db(db: TinyDB):
    all_skills = db.all()
    data = []
    for i in range(len(all_skills)):
        skill = SkillModel(
            name=all_skills[i]['name'],
            id=all_skills[i]['id'],
            YearsOfExp=all_skills[i]["YearsOfExp"],
            CVId=all_skills[i]['CVId'],
                     )
        skill = skill.dict(by_alias=True)
        data.append(skill)
    return data


def all_data_from_lang_skill_db(db: TinyDB):
    all_lang_skills = db.all()
    data = []
    for i in range(len(all_lang_skills)):
        lang_skill = LangSkillModel(
            name=all_lang_skills[i]['name'],
            id=all_lang_skills[i]['id'],
            level=all_lang_skills[i]['level'],
            CVId=all_lang_skills[i]['CVId'],
                     )
        lang_skill = lang_skill.dict(by_alias=True)
        data.append(lang_skill)
    return data


if __name__ == "__main__":
    print(all_data_from_skill_db(skill_db))
    print(all_data_from_lang_skill_db(lang_skill_db))
    print(all_data_from_cv_db(cv_db))
