# few json objects for each of three table

response_cv = {
    "items": [
        {
            "id": "1",
            "name": "vvv333",
            "description": "wannabe dev",
            "CareerStart": "01 01 2021",
            "PDF": "www.vvv333dev@gmail.com"
         },
    ],
    "synchronizationType": "full"
}

response_cv_skill = {
    "items": [
        {
            "id": '1',
            "name": "Python",
            "YearsOfExp": "1",
            "CVId": "1"
        }
    ],
    "synchronizationType": "full"
}

response_lang_skill = {
    "items": [
        {
            "id": '1',
            "name": "English",
            "level": "B2",
            "CVId": "1"
        }
    ],
    "synchronizationType": "full"
}
