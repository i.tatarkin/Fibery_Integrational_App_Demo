from tinydb import TinyDB


# script that create 3 db tables and feed data into it
cv_db = TinyDB("cv_db.json")
skill_db = TinyDB("skill_db.json")
lang_skill_db = TinyDB("lang_skill_db.json")


if __name__ == "__main__":
    cvs = [
        {
            "id": "1",
            "name": "Ivan Tatarkin",
            "description": "wannabe dev",
            "CareerStart": "01 01 2021",
            "PDF": "www.vvv333dev@gmail.com"
         },
        {
            "id": "2",
            "name": "Vovchik Idontknowhislastname",
            "description": "strong junior/pre-middle",
            "CareerStart": "01 01 2020",
            "PDF": "www.vovchik@gmail.com"
         },
        {
            "id": "3",
            "name": "Rodion Postanogov",
            "description": "Already hired (congrats)",
            "CareerStart": "01 01 2021",
            "PDF": "www.rodik@gmail.com"
         }
    ]
    for i in cvs:
        cv_db.insert(i)

    skills = [
        {
            "id": '1',
            "name": "Python",
            "YearsOfExp": "1",
            "CVId": "1"
        },
        {
            "id": '2',
            "name": "Django",
            "YearsOfExp": "1",
            "CVId": "1"
        },
        {
            "id": '3',
            "name": "FastAPI",
            "YearsOfExp": "1",
            "CVId": "1"
        },
        {
            "id": '4',
            "name": "Python",
            "YearsOfExp": "1",
            "CVId": "2"
        },
        {
            "id": '5',
            "name": "Django",
            "YearsOfExp": "1",
            "CVId": "2"
        },
        {
            "id": '6',
            "name": "Docker",
            "YearsOfExp": "1",
            "CVId": "2"
        },
        {
            "id": '7',
            "name": "Python",
            "YearsOfExp": "1",
            "CVId": "3"
        },
        {
            "id": '8',
            "name": "Django",
            "YearsOfExp": "1",
            "CVId": "3"
        },
        {
            "id": '9',
            "name": "PostgreSQL",
            "YearsOfExp": "1",
            "CVId": "3"
        }
    ]
    for i in skills:
        skill_db.insert(i)

    lang_skills = [
        {
            "id": '1',
            "name": "English",
            "level": "A1",
            "CVId": ""
        },
        {
            "id": '2',
            "name": "English",
            "level": "A2",
            "CVId": ""
        },
        {
            "id": '3',
            "name": "English",
            "level": "B1",
            "CVId": "2"
        },
        {
            "id": '4',
            "name": "English",
            "level": "B2",
            "CVId": "[1, 3]"
        },
        {
            "id": '5',
            "name": "English",
            "level": "C1",
            "CVId": ""
        },
        {
            "id": '6',
            "name": "English",
            "level": "C2",
            "CVId": ""
        }
    ]
    for i in lang_skills:
        lang_skill_db.insert(i)
