from fastapi import FastAPI, Response, status
from data import app_info, schema
from starlette.status import HTTP_204_NO_CONTENT
from models.validate_request_model import ValidateRequestModel
from models.sync_data_models import SyncDataRequest
from db.real_db import cv_db, skill_db, lang_skill_db, \
    all_data_from_cv_db, all_data_from_skill_db, all_data_from_lang_skill_db

app = FastAPI()


# return main app information
@app.get("/")
async def app_info_response():
    return app_info


# return logo (no logo)
@app.get("/logo", status_code=HTTP_204_NO_CONTENT, response_class=Response)
async def logo():
    pass


# hardcode token emulation (actually dont need any stronger system)
tokens = {
    "123": "Sergey Bershadskiy",
    "234": "Alena Bulgakova",
    "345": "Polina Paulina"
}


@app.post("/validate")
def validate(request: ValidateRequestModel, response: Response):
    token_from_request = request.fields['token']
    if token_from_request not in tokens.keys():
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return {"message": "Your password is incorrect!"}
    return {"name": tokens[token_from_request]}


@app.post("/api/v1/synchronizer/config")
async def sync_conf():
    return {
        "types": [
            {
                "id": "CV", "name": "CV"
            },
            {
                "id": "CVSkill", "name": "CVSkill"
            },
            {
                "id": "LanguageSkill", "name": "LanguageSkill"
            }
            ],
        "filters": [],
    }


@app.post("/api/v1/synchronizer/schema")
async def sync_schema():
    return schema


@app.post("/api/v1/synchronizer/data")
async def return_data(request: SyncDataRequest):
    if request.requested_type == "CV":
        datalist = all_data_from_cv_db(cv_db)
    elif request.requested_type == "CVSkill":
        datalist = all_data_from_skill_db(skill_db)
    elif request.requested_type == "LanguageSkill":
        datalist = all_data_from_lang_skill_db(lang_skill_db)
    else:
        return {
            "message": "Unable to fetch data."
        }
    return {
        "items": datalist,
        "synchronizationType": "full"
    }
