from pydantic import BaseModel
from typing import Optional


class SyncRequest(BaseModel):
    account: Optional[dict]
    types: list[str]
    filter: Optional[dict]


class SyncResponse(BaseModel):
    types: Optional[list[dict]]
    filters: Optional[list[dict]]
    webhooks: Optional[dict]
