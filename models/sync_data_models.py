from pydantic import BaseModel, Field
from typing import Optional


class SyncDataRequest(BaseModel):
    requested_type: str = Field(alias="requestedType")
    types: list[str]
    filter: dict
    account: dict
    pagination: Optional[dict]
    last_sync_at: Optional[str] = Field(alias="lastSynchronizedAt")
