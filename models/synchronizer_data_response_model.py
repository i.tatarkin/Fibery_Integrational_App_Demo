from pydantic import BaseModel, Field
from db.real_db import all_data_from_cv_db
from tinydb import TinyDB

cv_db = TinyDB()
print(cv_db.all())


class SyncDataResponse(BaseModel):
    items: list[dict]
    synchro_type: str = Field(alias="synchronizationType", default="full")


items = SyncDataResponse(items=all_data_from_cv_db(cv_db))
print(items)
