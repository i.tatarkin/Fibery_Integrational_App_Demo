from pydantic import BaseModel, Field


class CVModel(BaseModel):
    id: str
    name: str
    description: str
    career_start: str = Field(alias="CareerStart")
    pdf_link: str = Field(alias='PDF')


class SkillModel(BaseModel):
    id: str
    name: str
    years_of_exp: str = Field(alias="YearsOfExp")
    cv_id: int = Field(alias='CVId')


class LangSkillModel(BaseModel):
    id: str
    name: str
    level: str
    cv_id: str = Field(alias="CVId")
