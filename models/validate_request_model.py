from pydantic import BaseModel


class ValidateRequestModel(BaseModel):
    id: str
    fields: dict


data = {
    "id": "token",
    "fields": {
            "_id": "612f626611b067ec6246fa63",
            "app": "612f6261b6402f0d094c7bb3",
            "auth": "token",
            "owner": "611e0c70d4feec367b037b5b",
            "token": "123",
            "enabled": True,
            "name": "Accepted account",
            "masterAccountId": None,
            "lastUpdatedOn": "2021-09-02T11:33:11.701Z"
    }
}

test = ValidateRequestModel(**data)
print(test.fields['token'])
