app_info = {
    "id": "CVPortalSync2",
    "version": "1.1",
    "type": "crunch",
    "name": "CVPortalSyncSample2",
    "description": "Sample app2",
    "authentication": [
        {
            "id": "token",
            "name": "Token Authentication",
            "description": "Provide Token",
            "fields": [
                {
                    "id": "token",
                    "name": "some name",
                    "description": "Personal Token",
                    "type": "text",
                },
            ]
        }
    ],
    "sources": [],
    "responsibleFor": {
        "dataSynchronization": True,
        "dataProviding": True
        }
    }


schema = {
  "CV": {
    "id": {
      "type": "id",
      "name": "Id"
    },
    "name": {
      "type": "text",
      "name": "Name"
    },
    "description": {
      "type": "text",
      "name": "Description"
    },
    "CareerStart": {
      "type": "date",
      "name": "CareerStart"
    },
    "PDF": {
      "type": "text",
      "name": "PDF",
      "subType": "url"
    }
  },
  "CVSkill": {
    "id": {
      "type": "id",
      "name": "Id"
    },
    "name": {
      "type": "text",
      "name": "Name"
    },
    "YearsOfExp": {
        "name": "Years of experience",
        "type": "number"
    },
    "CVId": {
        "type": "text",
        "name": "CV Id",
        "relation": {
            "cardinality": "many-to-one",
            "name": "CV",
            "targetName": "Skills",
            "targetType": "CV",
            "targetFieldId": "id"
        }
    },
    "__syncAction": {
      "type": "text",
      "name": "Sync Action"
    }
  },
  "LanguageSkill": {
      "id": {
          "type": "id",
          "name": "id"
      },
      "name": {
          "type": "text",
          "name": "Name"
      },
      "level": {
          "name": "Level",
          "type": "text"
      },
      "CVId": {
          "type": "text",
          "name": "CV Id",
          "relation": {
              "cardinality": "many-to-many",
              "name": "CV",
              "targetName": "LanguageSkills",
              "targetType": "CV",
              "targetFieldId": "id"
          }
      },
      "__syncAction": {
          "type": "text",
          "name": "Sync Action"
          }
  }
}
